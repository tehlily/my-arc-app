import mongoose from 'mongoose';

// console.log("Starting database connection from outside of function");
let isConnected = false;
// console.log("Connected? " + isConnected);

export const connectToDb = async (cb) => {
    // console.log("Starting database connection");
    if (isConnected) {
        // console.log("Already connected to MongoDB");
        return;
    }

    try {
        const mongoPassword = process.env.MONGO_PASSWORD;
        const mongoUsername = "ARCUser";

        const mongoUri = `mongodb+srv://${mongoUsername}:${mongoPassword}@cluster0.mr4kmsu.mongodb.net/arcdata?retryWrites=true&w=majority&appName=Cluster0`;

        await mongoose.connect(mongoUri, { useNewUrlParser: true, useUnifiedTopology: true });
        isConnected = true;
        console.log("Connected to MongoDB");
        cb();
    } catch (error) {
        console.error("Error connecting to MongoDB: ", error);
        throw error;
    }
};


