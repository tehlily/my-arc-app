import nodemailer from 'nodemailer';

// var transporter = nodemailer.createTransport({
//     host: "sandbox.smtp.mailtrap.io",
//     port: 2525,
//     auth: {
//       user: "4469ed7f5eb038",
//       pass: "7ef371e657d62a"
//     }
//   });

const transporter = nodemailer.createTransport({
    host: 'live.smtp.mailtrap.io',
    port: 587,
    secure: false, //use SSL
    auth: {
        user: 'api',
        pass: '608fe523488f8c4398351acb7d3720be',
    }
});

const sendEmail = (req, res) => {
    const { contactName, contactEmail, contactMessage } = req.body;
    const mailOptions = {
        from: contactEmail,
        to: 'tehlily@gmail.com',
        subject: `Message from ${contactName}`,
        text: contactMessage
    };

    transporter.sendMail(mailOptions, (error, info) => {
        if (error) {
            // console.log('Error:', error);
            res.status(500).send('Failed to send email');
        } else {
            // console.log('Email sent:', info.response);
            res.status(200).send('Email sent successfully');
        }
    });
};

export default sendEmail;