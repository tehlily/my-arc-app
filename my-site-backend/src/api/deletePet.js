import { connectToDb } from '../dbConn.js';
import cors from 'cors';
import { ObjectId } from 'mongodb';
import PetRecord from '../models/PetRecord.js';
import { Storage } from '@google-cloud/storage';
import path from 'path';
import { fileURLToPath } from 'url';

const __filename = fileURLToPath(import.meta.url);
const __dirname = path.dirname(__filename);

const storage = new Storage({
    keyFilename: path.join(__dirname, '../../my-react-site-6f999-a34d8bdf540f.json'),
    projectId: 'my-react-site-6f999', 
});

const bucketName = 'arc_photos_1';
const bucket = storage.bucket(bucketName);

const corsMiddleware = cors({ origin: true });

const deleteFile = async (fileName) => {
    try {
        await bucket.file(fileName).delete();
        // console.log(`File ${fileName} deleted successfully.`);
    } catch (error) {
        console.error(`Failed to delete file ${fileName}:`, error);
    }
};

const deletePet = async (req, res) => {
    const { id } = req.params;

    // console.log("Id using req.params on deletePet:", id);

    corsMiddleware(req, res, async () => {
        // console.log("Starting deletePet from outside try");
        try {
            // console.log("Starting deletePet from inside try");
            const objectId = new ObjectId(id);
            await connectToDb();
            const pet = await PetRecord.deleteOne({ _id: objectId });
            if (pet) {
                if (pet.petImage) {
                    await deleteFile(pet.petImage);
                }
                res.json(pet);
            } else {
                res.sendStatus(404);
            }
        } catch (error) {
            console.error("Error deleting pet", error);
            res.status(500).send("Error deleting pet");
        }
    });
};

export default deletePet;
