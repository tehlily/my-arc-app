import { connectToDb } from '../dbConn.js';
import PetRecord from '../models/PetRecord.js';
import axios from 'axios';
import cors from 'cors';

const corsMiddleware = cors({ origin: true });
// console.log("Starting getPets from outside function");
const getMap = async (req, res) => {
    corsMiddleware(req, res, async () => {
        // console.log("Starting getMap from outside try");
        try {
            // console.log("Starting getMap from inside try");
            await connectToDb();

            const cities = await PetRecord.aggregate([
                {
                    $group: {
                        _id: { city: `$rescueCity`, state: `$rescueState` },
                        pets: { $push: `$$ROOT` }
                    }
                }
            ]);
            for (const city of cities) {
                try {
                    // const coordinates = await axios.get(`https://maps.googleapis.com/maps/api/geocode/json?address=${city._id.city}+${city._id.state}&key=AIzaSyA12d5Q2uzAjEy94ObPIDm1Td0cO76bZj4`);
                    const response = await axios.get(`https://maps.googleapis.com/maps/api/geocode/json`, {
                        params: {
                            address: `${city._id.city}, ${city._id.state}`,
                            key: 'AIzaSyA12d5Q2uzAjEy94ObPIDm1Td0cO76bZj4' 
                        }
                    });
                    // console.log(response.data.results[0].geometry.location);
                    const coordinates = response.data.results[0].geometry.location;
                    city.cityLatLng = { lat: coordinates.lat, lng: coordinates.lng };
                }catch (error) {
                    console.error(`Error converting city ${city._id.city}, ${city._id.state} to coordinates:`, error);
                    city.cityLatLng = null;
                }
            }
            res.json(cities);
        } catch (error) {
            console.error("Error fetching cities:", error);
            res.status(500).send("Error fetching cities");
        }
    });
};

export default getMap;
