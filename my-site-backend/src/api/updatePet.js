import { connectToDb } from '../dbConn.js';
import multer from 'multer';
import { Storage } from '@google-cloud/storage';
import cors from 'cors';
import PetRecord from '../models/PetRecord.js';
import path from 'path';
import { fileURLToPath } from 'url';

const __filename = fileURLToPath(import.meta.url);
const __dirname = path.dirname(__filename);

const storage = new Storage({
    keyFilename: path.join(__dirname, '../../my-react-site-6f999-a34d8bdf540f.json'),
    projectId: 'my-react-site-6f999', 
});

const bucketName = 'arc_photos_1';
const bucket = storage.bucket(bucketName);

const upload = multer({
    storage: multer.memoryStorage(),
    limits: { fileSize: 1024 * 1024 * 5 } // 5 MB file size limit
});

const corsMiddleware = cors({ origin: true });

const uploadImageToGCS = (file) => {
    return new Promise((resolve, reject) => {
        const blob = bucket.file(file.originalname);
        const blobStream = blob.createWriteStream({
            resumable: false,
        });

        blobStream.on('error', (err) => {
            reject(err);
        });
        blobStream.on('finish', () => {
            const publicUrl = `${blob.name}`;
            resolve(publicUrl);
        });

        blobStream.end(file.buffer);
    });
};

// console.log("Starting updatePet");

const updatePet = async (req, res) => {
    const { id } = req.params;

    corsMiddleware(req, res, async () => {
        upload.single('petImage')(req, res, async (uploadError) => {
            if (uploadError) {
                console.error("Error uploading file:", uploadError);
                return res.status(400).send("Error uploading file");
            }
            try {
                const sanitizedBody = Object.fromEntries(
                    Object.entries(req.body).filter(([_, v]) => v != null && v !== 'null')
                );
                // Handle the arrivalDate separately to ensure default value is used if not provided
                if (!sanitizedBody.arrivalDate) {
                    delete sanitizedBody.arrivalDate;
                };

                let publicUrl = null;
                if (req.file) {
                    publicUrl = await uploadImageToGCS(req.file);
                    sanitizedBody.petImage = publicUrl;
                }

                await connectToDb();
                const updatedPetRecord = await PetRecord.findByIdAndUpdate(id, sanitizedBody, { new: true });

                if (!updatedPetRecord) {
                    return res.status(404).json({ message: 'Pet record not found' });
                }

                res.status(200).json({ message: 'Pet record updated successfully', pet: updatedPetRecord });

            } catch (error) {
                console.error("Error updating pet record: ", error);
                res.status(500).send("Error updating pet record");
            }
        });
    });
};

export default updatePet;
