import cities from '../models/arcdata.cities.js';

const validateLocation = async (req, res) => {
    // console.log("Starting validation");
    const { city, state } = req.body;
    // console.log("city, state passed to validateLocation:", city, state)


    const validCity = cities.find(entry => entry.city === city && entry.state_name === state);

    if (validCity) {
        // console.log("Valid City: ", validCity);
        res.json({valid: true});
    } else {
        // console.log("Undefined:", validCity);
        res.json({valid: false});
    }
    
}

export default validateLocation;