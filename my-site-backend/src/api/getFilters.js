import { connectToDb } from '../dbConn.js';
import PetRecord from '../models/PetRecord.js';
import cors from 'cors';

const corsMiddleware = cors({ origin: true });

const getPetFilters = async (req, res) => {
    const friendlies = ["Dogs", "Cats", "Kids"];
    const sexes = ["Male", "Female", "Unknown"];
    const altereds = ["Yes", "No", "Unknown"];
    corsMiddleware(req, res, async () => {
        try {
            await connectToDb();
            const specieses = await PetRecord.distinct('species');
            const ageNums = await PetRecord.aggregate([
                { $match: { ageUnit: 'Years' } },
                { $group: { _id: "$ageNum" } },
                { $sort: { _id: 1 } }
            ]).exec();
            
            const uniqueAgeNums = ageNums.map(age => age._id);
            const totalAgeNums = ["Under a year", ...uniqueAgeNums];
            const petRescues = await PetRecord.distinct('petRescue');
            const rescueLocals = await PetRecord.aggregate([
                {
                    $group: {
                        _id: { city: "$rescueCity", state: "$rescueState" }
                    }
                },
                {
                    $project: {
                        _id: 0,
                        city: "$_id.city",
                        state: "$_id.state"
                    }
                },
                {
                    $sort: { city: 1, state: 1 }
                }
            ]).exec();

            res.status(200).json({ 
                ageNums: totalAgeNums,
                specieses, 
                rescueLocals,
                friendlies,
                petRescues, 
                sexes,
                altereds,
            });
        } catch (error) {
            console.error("Error fetching filter values:", error);
            res.status(500).json({ message: 'Error fetching filter values', error });
        }
    });
};

export default getPetFilters;
