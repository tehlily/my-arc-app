import { connectToDb } from '../dbConn.js';
import cors from 'cors';
import { ObjectId } from 'mongodb';
import PetRecord from '../models/PetRecord.js';
import { Storage } from '@google-cloud/storage';
import path from 'path';
import { fileURLToPath } from 'url';

const __filename = fileURLToPath(import.meta.url);
const __dirname = path.dirname(__filename);

const storage = new Storage({
    keyFilename: path.join(__dirname, '../../my-react-site-6f999-a34d8bdf540f.json'),
    projectId: 'my-react-site-6f999', 
});

const bucketName = 'arc_photos_1';
const bucket = storage.bucket(bucketName);

const corsMiddleware = cors({ origin: true });

const getSignedUrl = async (fileName) => {
    const options = {
        version: 'v4',
        action: 'read',
        expires: Date.now() + 15 * 60 * 1000, // 15 minutes
    };

    const [url] = await bucket.file(fileName).getSignedUrl(options);
    return url;
};

const getPetID = async (req, res) => {
    const { id } = req.params;

    // console.log("Id using req.params on getPetID:", id);

    corsMiddleware(req, res, async () => {
        // console.log("Starting getPetID from outside try");
        try {
            // console.log("Starting getPetID from inside try");
            const objectId = new ObjectId(id);
            await connectToDb();
            const pet = await PetRecord.findOne({ _id: objectId });
            if (pet) {
                if (pet.petImage) {
                    const signedUrl = await getSignedUrl(pet.petImage);
                    pet.petImage = signedUrl;
                }
                res.json(pet);
            } else {
                res.sendStatus(404);
            }
        } catch (error) {
            console.error("Error finding pet", error);
            res.status(500).send("Error finding pet");
        }
    });
};

export default getPetID;
