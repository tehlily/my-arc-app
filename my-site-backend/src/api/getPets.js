import { connectToDb } from '../dbConn.js';
import PetRecord from '../models/PetRecord.js';
import cors from 'cors';
import { Storage } from '@google-cloud/storage';
import path from 'path';
import { fileURLToPath } from 'url';

const __filename = fileURLToPath(import.meta.url);
const __dirname = path.dirname(__filename);

const storage = new Storage({
    keyFilename: path.join(__dirname, '../../my-react-site-6f999-a34d8bdf540f.json'),
    projectId: 'my-react-site-6f999', 
});

const bucketName = 'arc_photos_1';
const bucket = storage.bucket(bucketName);

const corsMiddleware = cors({ origin: true });

const getSignedUrl = async (fileName) => {
    const options = {
        version: 'v4',
        action: 'read',
        expires: Date.now() + 15 * 60 * 1000, // 15 minutes
    };

    const [url] = await bucket.file(fileName).getSignedUrl(options);
    return url;
};

const getPets = async (req, res) => {
    const filters = req.query;
    let query = {};

    if (filters.age) {
        if (filters.age === 'Under a year') {
            query.ageUnit = { $ne: 'Years'};
        } else {
            query.ageNum = filters.age;
            query.ageUnit = 'Years';
        }
    }    
    if (filters.species) query.species = filters.species;
    if (filters.sex) query.sexGen = filters.sex;    
    if (filters.altered) query.sexAlt = filters.altered;    
    if (filters.dogFriendly) query.dogFriendly = filters.dogFriendly;
    if (filters.catFriendly) query.catFriendly = filters.catFriendly;
    if (filters.kidFriendly) query.kidFriendly = filters.kidFriendly;
    if (filters.rescue) query.petRescue = filters.rescue;
    if (filters.rescueCity) query.rescueCity = filters.rescueCity;
    if (filters.rescueState) query.rescueState = filters.rescueState;

    corsMiddleware(req, res, async () => {
        try {
            await connectToDb();
            const pets = await PetRecord.find(query);
            if (Array.isArray(pets)) {
                for (const pet of pets) {
                    if (pet.petImage) {
                        const signedUrl = await getSignedUrl(pet.petImage);
                        pet.petImage = signedUrl;
                    } else {
                        pet.petImage = `../uploads/015-dog.png`;
                    }
                }
                res.status(200).send(pets);
            } else {
                res.status(404).send({ message: "Array not found" });
            }
        } catch (error) {
            console.error("Error fetching pets:", error);
            res.status(500).send("Error fetching pets");
        }
    });
};

export default getPets;
