import { connectToDb } from '../dbConn.js';
import multer from 'multer';
import { Storage } from '@google-cloud/storage';
import cors from 'cors';
import PetRecord from '../models/PetRecord.js'
import path from 'path';
import { fileURLToPath } from 'url';

const __filename = fileURLToPath(import.meta.url);
const __dirname = path.dirname(__filename);

const storage = new Storage({
    keyFilename: path.join(__dirname, '../../my-react-site-6f999-a34d8bdf540f.json'),
    projectId: 'my-react-site-6f999', 
});


// let storageOptions;

// if (process.env.NODE_ENV === 'production') {
    
//     storageOptions = {
//         keyFilename: '/secrets/gcloud.json',
//         projectId: 'my-react-site-6f999',
//     };
// } else {
//     storageOptions = {
//         keyFilename: '../my-site-backend/my-react-site-6f999-a34d8bdf540f.json',
//         projectId: 'my-react-site-6f999',
//     };

// }

// const storage = new Storage(storageOptions);

// const storage = new Storage({
//     keyFilename: '../my-site-backend/my-react-site-6f999-a34d8bdf540f.json',
//     projectId: 'my-react-site-6f999',
// });

const bucketName = 'arc_photos_1';
const bucket = storage.bucket(bucketName);

const upload = multer({ 
    storage: multer.memoryStorage(),
    limit: { fileSize: 1025 * 1024 * 5 } // 5 MB file size limit
});

const corsMiddleware = cors({ origin: true });

const uploadImageToGCS = (file) => {
    return new Promise((resolve, reject) => {
        const blob = bucket.file(file.originalname);
        const blobStream = blob.createWriteStream({
            resumable: false,
        });

        blobStream.on('error', (err) => {
            reject(err);
        });
        blobStream.on('finish', () => {
            const publicUrl = `${blob.name}`;
            resolve(publicUrl);
        });

        blobStream.end(file.buffer);
    });
};

// console.log("Starting postNewPet");

const postNewPet = async ( req, res) => {
    corsMiddleware(req, res, async () => {
        upload.single('petImage')(req, res, async (uploadError) => {
            if (uploadError) {
                console.error("Error uploading file:", uploadError);
                return res.status(400).send("Error uploading file");
            }
            try {
                const sanitizedBody = Object.fromEntries(
                    Object.entries(req.body).filter(([_, v]) => v != null && v !== 'null')
                );
                 // Handle the arrivalDate separately to ensure default value is used if not provided
                if (!sanitizedBody.arrivalDate) {
                    delete sanitizedBody.arrivalDate;
                };

                if (!sanitizedBody.species) {
                    delete sanitizedBody.species;
                };
                if (!sanitizedBody.color) {
                    delete sanitizedBody.color;
                };
                if (!sanitizedBody.breed) {
                    delete sanitizedBody.breed;
                };
                if (!sanitizedBody.breed) {
                    delete sanitizedBody.breed;
                };

                let publicUrl = null;
                if(req.file) {
                    publicUrl = await uploadImageToGCS(req.file);
                }
                const newPetRecord = new PetRecord({
                    ...sanitizedBody,
                    petImage: publicUrl,
                });
                await connectToDb();
                await newPetRecord.save();

                res.status(201).json({ message: 'New pet record created successfully', pet: newPetRecord });
                
            }catch (error) {
                console.error("Error creating new pet record: ", error);
            }
        });
    });
};

export default postNewPet;