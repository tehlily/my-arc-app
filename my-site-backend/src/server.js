import express from 'express';
import { connectToDb } from './dbConn.js';
// import { ObjectId } from 'mongodb';
import 'dotenv/config';
import fs from 'fs';
import path from 'path';
import admin from 'firebase-admin';
// import * as firebase from 'firebase/app';
import 'firebase/functions';
import cors from 'cors';
import getPetID from './api/getPetID.js';
import getPets from './api/getPets.js';
import getPetFilters from './api/getFilters.js';
import postNewPet from './api/postNewPet.js';
import deletePet from './api/deletePet.js';
import updatePet from './api/updatePet.js';
import sendEmail from './api/sendEmail.js';
import getMap from './api/getMap.js';
import validateLocation from './api/validateLocation.js';
import { fileURLToPath } from 'url';
const __filename = fileURLToPath(import.meta.url);
const __dirname = path.dirname(__filename);
import dotenv from 'dotenv';

dotenv.config();

// console.log("Starting backend server file");

const credentials = JSON.parse(
    fs.readFileSync('./credentials.json')
);
admin.initializeApp({
    credential: admin.credential.cert(credentials),
});

const app = express();
app.use(express.json());
app.use(express.static(path.join(__dirname, '../build')));

// Configure header info
app.use(cors({
    origin: true,
    methods: ["GET", "POST", "PUT", "PATCH", "DELETE"],
    // credentials: true,
}));

app.get(/^(?!\/api).+/, (req, res) => {
    res.sendFile(path.join(__dirname, '../build/index.html'));
})

app.use(async (req, res, next) => {
    const { authtoken } = req.headers;
    
    if (authtoken) {
        try {
            req.user = await admin.auth().verifyIdToken(authtoken);            
        } catch (e) {
            return res.sendStatus(400);
        }
    }
    req.user = req.user || {};
    
    next();          
});

// const firebaseAPI = process.env.YOUR_API_KEY;
// const firebaseID = process.env.YOUR_PROJECT_ID;

// const firebaseConfig = {
//     apiKey: firebaseAPI,
//     authDomain: `${firebaseID}.firebaseapp.com`,
//     databaseURL: `https://${firebaseID}.firebaseio.com`,
//     projectId: firebaseID,
// }

// firebase.initializeApp(firebaseConfig);


app.use((req, res, next) => {
    if (req.user) {
        next();
    } else {
        res.sendStatus(401);
    }
});

// console.log("Env: ", process.env.NODE_ENV);
let PORT;
if (process.env.NODE_ENV !== 'production') {
    PORT = 8080;
} else {
    PORT = process.env.PORT || 8080;  // Default to 8080 if PORT is not set
}
// app.listen(PORT, () => {
//         console.log(`Server is running on port ${PORT}`);
// });

export default app;
        
// const PORT = process.env.PORT || 8080;
// console.log("PORT: " + port);

connectToDb(() => {
        console.log('Successfully connected to database')
        app.listen(PORT, () => {
                console.log('Server is listening on port ' + PORT);
        })
});

app.get('/api/pets', getPets);

app.get('/api/pets/:id', getPetID);

app.post('/api/pets', postNewPet);

app.get('/api/pet_filters', getPetFilters);

app.delete('/api/pets/:id', deletePet);

app.put('/api/pets/:id', updatePet);

app.post('/api/send_email', sendEmail);

app.post('/api/validate_location', validateLocation);

app.get('/api/map', getMap);

// app.put('/api/pets/:id/upvote', async (req, res) => {
    //     const { id } = req.params;
    //     const { uid } = req.user;;

//     if (!uid) {
//         return res.sendStatus(401);
//     }

//     const objectId = new ObjectId(id);

//     const pet = await connectToDb.collection('petrecords').findOne({ _id: objectId  });

//     if (pet) {
//         const upvoteIds = pet.upvoteIds || [];
//         const canUpvote = uid && !upvoteIds.includes(uid);

//         if (canUpvote) {
//             await connectToDb.collection('petrecords').updateOne({ _id: objectId  }, {
//                 $inc: { upvotes: 1 },
//                 $push: { upvoteIds: uid },
//             });
//         }
//         const updatedPet = await connectToDb.collection('petrecords').findOne({ _id: objectId });
//         res.json(updatedPet);
//     } else {
//         res.send('That pet doesn\'t exist');
//     }
// });

// app.post('/api/pets/:id/comments', async (req, res) => {
//     const { id } = req.params;
//     const objectId = new ObjectId(id);
//     const { text} = req.body;
//     const { email } = req.user;

//     if (!email) {
//         return res.sendStatus(401);
//     }

//     await connectToDb.collection('petrecords').updateOne({  _id: objectId }, {
//         $push: { comments: { postedBy: email, text } },
//     });
//     const pet = await connectToDb.collection('petrecords').findOne({  _id: objectId });

//     if (pet) {        
//         res.json(pet);
//     } else {
//         res.send("That pet doesn't exist!");
//     }

// });