import mongoose from "mongoose";

const petRecordSchema = new mongoose.Schema({
    name: {
        type: String,
        required: [true, "Name is required"]
    },
    species: {
        type: String,
        required: true,
        default: 'Unknown'
    },
    breed: {
        type: String,
        required: true,
        default: 'Unknown'
    },
    color: {
        type: String,
        required: true,
        default: 'Unknown'
    },
    ageNum: {
        type: Number,
        required: [true, "Age is required"]
    },
    ageUnit: {
        type: String,
        required: [true, "Age unit is required"],
        default: 'Unknown'
    },
    sexGen: {
        type: String,
        required: false,
        default: 'Unknown'
    },
    sexAlt: {
        type: String,
        required: false,
        default: 'Unknown'
    },
    dogFriendly: {
        type: String,
        required: false,
        default: 'Unknown'
    },
    catFriendly: {
        type: String,
        required: false,
        default: 'Unknown'
    },
    kidFriendly: {
        type: String,
        required: false,
        default: 'Unknown'
    },
    petRescue: {
        type: String,
        required: [true, "Rescue Name is required"]
    },
    rescueEmail: {
        type: String,
        required: [true, "Rescue Email is required"]
    },
    rescueCity: {
        type: String,
        required: [true, "City is required"]
    },
    rescueState: {
        type: String,
        required: [true, "State is required"]
    },
    arrivalDate: {
        type: Date,
        required: true,
        default: () => new Date(+new Date() + 7*24*60*60*1000)
    },
    petImage: String,
}, { 
    collection: "petrecords",
    timestamps: true });

petRecordSchema.virtual('formattedArrivalDate').get(function() {
    const options = { weekday: 'long', year: 'numeric', month: 'long', day: 'numeric' };
    return this.arrivalDate.toLocaleDateString('en-US', options);
});

petRecordSchema.set('toJSON', {virtuals: true });
petRecordSchema.set('toObject', {virtuals: true });

const PetRecord = mongoose.model("PetRecord", petRecordSchema);

export default PetRecord;
// module.exports = PetRecord;
