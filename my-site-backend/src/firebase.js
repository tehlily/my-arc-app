import firebase from 'firebase/app';
import 'firebase/functions';

const firebaseAPI = process.env.YOUR_API_KEY;
const firebaseID = process.env.YOUR_PROJECT_ID;

const firebaseConfig = {
    apiKey: firebaseAPI,
    authDomain: `${firebaseID}.firebaseapp.com`,
    databaseURL: `https://${firebaseID}.firebaseio.com`,
    projectId: firebaseID,
    // storageBucket: "YOUR_PROJECT_ID.appspot.com",
    // messagingSenderId: "YOUR_SENDER_ID",
    // appId: "YOUR_APP_ID"
};

firebase.initializeApp(firebaseConfig);


