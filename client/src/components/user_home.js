import { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import { useCookies } from "react-cookie";
import axiosClient from "../axiosClient";
import { ToastContainer, toast } from "react-toastify";
import Button from "react-bootstrap/Button";
import { Link } from 'react-router-dom';

const Home = () => {
  const navigate = useNavigate();
  const [cookies, removeCookie] = useCookies([]);
  const [username, setUsername] = useState("");

  useEffect(() => {
    const verifyToken = async () => {
      if (!cookies.token) {
        navigate("/login");
      } else {
        try {
          const { data } = await axiosClient.get("/api/user_data", {
            withCredentials: true,
          });
          setUsername(data.user);
        } catch (error) {
          console.error("Error fetching user data:", error);
          toast.error("Error fetching user data", {
            position: "bottom-left",
          });
          removeCookie("token");
          navigate("/login");
        }
      }
    };

    verifyToken();
  }, [cookies, navigate, removeCookie]);

  const handleLogout = () => {
    removeCookie("token");
    navigate("/login");
  };

  return (
    <>
      <div className="home_page">
        <h2>
          {" "}
          Welcome <span>{username}</span>
        </h2>
        <Link to="/pet_record_list" className="spaced">
          <Button>View All Pets</Button>
        </Link>
        <Link to="/create_pet" className="spaced">
          <Button>Add A Pet</Button>
        </Link>
        <Link to="/contact" className="spaced">
          <Button>Contact Us</Button>
        </Link>
        <button onClick={handleLogout}>LOGOUT</button>
      </div>
      <ToastContainer />
    </>
  );
};

export default Home;