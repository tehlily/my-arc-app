import React, { useState, useEffect } from "react";
import { useParams, useNavigate } from "react-router-dom";
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import axiosClient from '../axiosClient';


export default function ViewPet() {
    const [petRecord, setPetRecord] = useState({});
    const params = useParams();
    const navigate = useNavigate();

    useEffect(() => {
        async function getPet() {
            try {
                const response = await axiosClient.get(`/api/view_pet/${params.id}`);
                setPetRecord(response.data);
            } catch (error) {
                console.error("An error occurred while fetching the pet record:", error);
                window.alert("An error occurred while fetching the pet record.");
                navigate("/pet_record_list");
            }
        }
        getPet();
    }, [params.id, navigate]);

    return (
        <div>
            <h3>{petRecord.name}'s Information</h3>
            <Container>
                <Row>
                    <Col>
                        <p><strong>Pet Name:</strong> {petRecord.name}</p>
                        <p><strong>Pet Species:</strong> {petRecord.species ? (
                            `${petRecord.species}` ) : (`Unknown`)
                        }</p>
                        <p><strong>Pet Breed:</strong> {petRecord.breed ? (
                            `${petRecord.breed}` ) : (`Unknown`)
                        } </p>
                        <p><strong>Pet Color:</strong> {petRecord.color ? (
                            `${petRecord.color}` ) : (`Unknown`)
                        }</p>

                        <p><strong>Pet Age:</strong> {petRecord.ageNum} {petRecord.ageUnit}</p>
                        <p><strong>Pet Sex:</strong> {petRecord.sexGen}</p>
                        <p><strong>Is Pet Altered?</strong> {petRecord.sexAlt}</p>
                        <p><strong>Is Pet Friendly with Dogs?</strong> {petRecord.dogFriendly ? (
                            `${petRecord.dogFriendly}` ) : (`Unknown`)
                        }</p>
                        <p><strong>Is Pet Friendly with Cats?</strong> {petRecord.catFriendly ? (
                            `${petRecord.catFriendly}` ) : (`Unknown`)
                        }</p>
                        <p><strong>Is Pet Friendly with Kids?</strong> {petRecord.kidFriendly ? (
                            `${petRecord.kidFriendly}` ) : (`Unknown`)
                        }</p>
                        <p><strong>Rescue Name:</strong> {petRecord.petRescue}</p>
                        <p><strong>Rescue Email:</strong> {petRecord.rescueEmail && (
                <a href={`mailto:${petRecord.rescueEmail}`}>{petRecord.rescueEmail}</a>
                )}</p>
                    <p><strong>Foster needed by:
                    {petRecord.arrivalDate ? (
                            `${petRecord.arrivalDate}` ) : (`Unknown`)
                        }</strong></p>
                    </Col>
                    <Col>
                        {petRecord.petImage ? (
                            <img src={`../uploads/${petRecord.petImage}`} alt={`${petRecord.name}`} height="500"/>
                            ) : (
                                <img src={`../uploads/015-dog.png`} alt={`${petRecord.name}`} height="500"/> 
                            )}
                    </Col>
                </Row>
            </Container>
        </div>
    );
}