import React, { useEffect, useState } from "react";
import Row from 'react-bootstrap/Row';
import axiosClient from "../axiosClient";

import Button from 'react-bootstrap/Button';
import Card from 'react-bootstrap/Card';
import Col from 'react-bootstrap/Col';
import CardGroup from 'react-bootstrap/CardGroup';
import { Link } from "react-router-dom";
import { toast } from "react-toastify";
// import axios from 'axios';
// import PetRecord from "./PetRecord";

function PetRecord({ petrecord, deletePetRecord }) {
  const [isAuthenticated, setIsAuthenticated] = useState(false);

  useEffect(() => {
    const checkAuth = async () => {
      try {
        const { data } = await axiosClient.get("/api/user_data", {
          withCredentials: true,
        });
        setIsAuthenticated(data.status);
      } catch (error) {
        console.error("Error fetching user data:", error);
        toast.error("Error fetching user data", {
          position: "bottom-left",
        });
      }
    };
    checkAuth();
  }, []); // Add empty dependency array

  return (
    <Col key={petrecord._id} xs={12} sm={6} md={4} lg={3}>
      <CardGroup>
        <Card style={{ width: '18rem', margin: '10px' }}>
          <Link to={`/view_pet/${petrecord._id}`}>
            <div className="ratio ratio-1x1">
              <Card.Img
                variant="top"
                src={petrecord.petImage
                  ? `../uploads/${petrecord.petImage}`
                  : `../uploads/015-dog.png`}
                alt={petrecord.name}
              />
            </div>
          </Link>
          <Card.Body>
            <Card.Title>{petrecord.name}</Card.Title>
            <Card.Text>
              <ul>
                <li>{petrecord.ageNum} {petrecord.ageUnit}</li>
                <li>{petrecord.sexGen}</li>
                <li>Altered? {petrecord.sexAlt}</li>
                <li>
                  {petrecord.petRescue} {petrecord.rescueEmail && (
                    <a href={`mailto:${petrecord.rescueEmail}`}>{petrecord.rescueEmail}</a>
                  )}
                </li>
              </ul>
            </Card.Text>
            {isAuthenticated &&
              <Link className="btn btn-link" to={`/edit_pet/${petrecord._id}`}>
                <img src="../images/png/069-pencil.png" height="30px" alt={`${petrecord.name}`} />
              </Link>}
            <Link className="btn btn-link" to={`/view_pet/${petrecord._id}`}>
              <Button>View Details</Button>
            </Link>
            {isAuthenticated &&
              <img
                src="../images/png/041-bin.png"
                height="45px"
                alt="Delete"
                className="btn btn-link"
                onClick={() => deletePetRecord(petrecord._id)}
                style={{ cursor: 'pointer' }}
              />
            }
          </Card.Body>
        </Card>
      </CardGroup>
    </Col>
  );
}

export default function PetRecordList() {
  const [petrecords, setPetRecords] = useState([]);
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState(null);

  useEffect(() => {
    async function getPetRecords() {
      try {
        const response = await axiosClient.get("api/pet_record_list/");
        console.log("axiosClient:", axiosClient);
        console.log("Fetched data:", response.data); // Debugging log
        if (Array.isArray(response.data)) {
          setPetRecords(response.data);
        } else {
          throw new Error("Fetched data is not an array");
        }
      } catch (error) {
        console.error("An error occurred while fetching pet records:", error);
        setError("An error occurred while fetching pet records.");
      } finally {
        setLoading(false);
      }
    }
    getPetRecords();
  }, []);
async function deletePetRecord(id) {
    try {
      if (!/^\d+$/.test(id)) {
        throw new Error('Invalid ID format');
      }
      await axiosClient.delete(`/${id}`);

      setPetRecords(petrecords.filter((record) => record._id !== id));
    } catch (error) {
      console.error("An error occurred while deleting the pet record:", error);
      alert("An error occurred while deleting the pet record.");
    }
  }

  if (loading) {
    return <div>Loading...</div>;
  }

  if (error) {
    return <div>{error}</div>;
  }

  return (
    <div>
      <h2>Pets Looking for Fosters</h2>
      <Row xs={1} md={2} lg={4} className="g-4">
        {petrecords.map((petrecord) => (
          <PetRecord
            key={petrecord._id}
            petrecord={petrecord}
            deletePetRecord={deletePetRecord}
          />
        ))}
      </Row>
    </div>
  );
}
