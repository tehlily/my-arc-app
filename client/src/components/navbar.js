import React, { useState, useEffect } from "react";
import Container from 'react-bootstrap/Container';
import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';
 // We import NavLink to utilize the react router.
import { NavLink, useNavigate } from "react-router-dom";
import { useCookies } from 'react-cookie'; // Import useCookies hook
import axiosClient from '../axiosClient'; // Import axiosClient
import { toast } from "react-toastify";

 // Here, we display our Navbar
export default function NavigationBar() {
    const [isAuthenticated, setIsAuthenticated] = useState(false);
    const [cookies, removeCookie] = useCookies([]);
    const navigate = useNavigate();
    // const [username, setUsername] = useState("");

    useEffect(() => {
        const verifyToken = async () => {
            try {
                const { data } = await axiosClient.get("/api/user_data", {
                    withCredentials: true,
                });
                console.log("User name:" + data.username);
                // setUsername(data.username);
                setIsAuthenticated(data.status);
            } catch (error) {
                console.error("Error fetching user data:", error);
                toast.error("Error fetching user data", {
                    position: "bottom-left",
                });
                removeCookie("token");
            }
          };
    
        verifyToken();
      }, [cookies, navigate, removeCookie]);

    const handleLogout = () => {
        removeCookie("token");
        setIsAuthenticated(false);
    }
    return (
    <div>
            <Navbar collapseOnSelect expand="lg" style={{backgroundColor: "#96b6c5"}}>
                <Container>
                    <Navbar.Brand href="#home">
                        <h1> 
                            <span id="acronym">ARC</span>    
                            <span id="company">Animal Rescue Coordinator</span>
                        </h1>
                    </Navbar.Brand>
                    <Navbar.Toggle aria-controls="basic-nabar-nav"/>
                    <Navbar.Collapse id="basic-navbar-nav">
                        <Nav className="me-auto">
                            <Nav><NavLink to="/" className="nav-link bg-tertiary">Home</NavLink></Nav>
                            <Nav><NavLink to="/pet_record_list" className="nav-link bg-tertiary">View All Pets</NavLink></Nav>
                            {isAuthenticated && <Nav><NavLink to="/create_pet" className="nav-link bg-tertiary">Add A Pet</NavLink></Nav>}
                            <Nav><NavLink to="/contact" className="nav-link bg-tertiary">Contact Us</NavLink></Nav>
                            <Nav><NavLink to="/login" className="nav-link bg-tertiary">Log In to Account</NavLink></Nav>
                            <Nav><NavLink to="/signup" className="nav-link bg-tertiary">Sign Up for Account</NavLink></Nav>
                            {isAuthenticated ? <button className="nav-link bg-tertiary" onClick={handleLogout}>Logout</button> : <></>}
                        </Nav>
                    </Navbar.Collapse>                
                </Container>
            </Navbar>
        
    </div>
    );
}