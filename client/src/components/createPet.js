import React, { useState } from "react";
import { useNavigate } from "react-router-dom";
import axiosClient from "../axiosClient";
import FormInput from "./FormInput";
import RadioGroup from "./RadioGroup";

export default function Create() {
    // const defaultImage = "../images/no-image.png";
    const defaultAnswer = "Unknown";

    const [form, setForm] = useState({
        name: "",
        species: defaultAnswer,
        breed: defaultAnswer,
        color: defaultAnswer,
        ageNum: "",
        ageUnit: "",
        sexGen: defaultAnswer,
        sexAlt: defaultAnswer,
        dogFriendly: defaultAnswer,
        catFriendly: defaultAnswer,
        kidFriendly: defaultAnswer,
        petRescue: "",
        rescueEmail: "",
        arrivalDate: null,
        petImage: null,
    });

    const navigate = useNavigate();

    // These methods will update the state properties.
    const updateForm = (value) => {
        setForm((prev) => {
            return { ...prev, ...value };
        });
    };

    const onChangeFile = (e) => {
        setForm({
            ...form,
            petImage: e.target.files[0],
        });
    };

    // This function will handle the submission.
    const onSubmit = async (e) => {
        e.preventDefault();

        // Prepare form data
        const formData = new FormData();
        for ( const key in form) {
            formData.append(key, form[key]);
        }

        // When a post request is sent to the create url, we'll add a new pet record to the database.
        try {
            await axiosClient.post("/petrecord/add", formData, {
                headers: {
                    "Content-Type": "multipart/form-data",
                },
            });
            setForm({
                name: "",
                species: defaultAnswer,
                breed: defaultAnswer,
                color: defaultAnswer,
                ageNum: "",
                ageUnit: "",
                sexGen: defaultAnswer,
                sexAlt: defaultAnswer,
                dogFriendly: defaultAnswer,
                catFriendly: defaultAnswer,
                kidFriendly: defaultAnswer,
                petRescue: "",
                rescueEmail: "",
                arrivalDate: defaultAnswer,
                petImage: null,
            });
            navigate("/pet_record_list/");
        } catch (error) {
            window.alert(error);
        }
    };

    // This following section will display the form that takes the input from the user.
    return (
        <div>
            <h2>Add a Pet Looking for Foster</h2>
            <form onSubmit={onSubmit} encType="multipart/form-data">
                <FormInput
                    label="Pet Name:"
                    id="name"
                    value={form.name}
                    onChange={(e) => updateForm({ name: e.target.value })}
                />
                <FormInput  
                    label="Pet Species:"
                    id="species"
                    value={form.species}
                    onChange={(e) => updateForm({ species: e.target.value })}
                />
            
                <FormInput
                    label="Pet Breed:"
                    id="breed"
                    value={form.breed}
                    onChange={(e) => updateForm({ breed: e.target.value })}
                />
                        
                <FormInput
                    label="Pet Color:"
                    id="color"
                    value={form.color}
                    onChange={(e) => updateForm({ color: e.target.value })}
                />
                        
                <FormInput
                    label="Pet Age (Number):"
                    id="ageNum"
                    min="1"
                    step="1"
                    value={form.ageNum}
                    onChange={(e) => updateForm({ ageNum: e.target.value })}
                />
                        
                <RadioGroup
                    label="Pet Age Units: "
                    name="ageUnit"
                    options={[
                        { label: "Weeks", value: "Weeks" },
                        { label: "Months", value: "Months" },
                        { label: "Years", value: "Years" },
                    ]}
                    selectedValue={form.ageUnit}
                    onChange={(e) => updateForm({ ageUnit: e.target.value })}
                />

                <RadioGroup
                    label="Pet Sex: "
                    name="sexGen"
                    options={[
                        { label: "Male", value: "Male" },
                        { label: "Female", value: "Female" },
                        { label: "Unknown", value: "Unknown" }
                    ]}
                    selectedValue={form.sexGen}
                    onChange={(e) => updateForm({ sexGen: e.target.value })}
                />
                
                <RadioGroup
                    label="Pet Altered? "
                    name="sexAlt"
                    options={[
                        { label: "Yes", value: "Yes" },
                        { label: "No", value: "No" },
                        { label: "Unknown", value: "Unknown" }
                    ]}
                    selectedValue={form.sexAlt}
                    onChange={(e) => updateForm({ sexAlt: e.target.value })}
                />
                <RadioGroup
                    label="Is the pet friendly to dogs? "
                    name="dogFriendly"
                    options={[
                        { label: "Yes", value: "Yes" },
                        { label: "No", value: "No" },
                        { label: "Unknown", value: "Unknown" }
                    ]}
                    selectedValue={form.dogFriendly}
                    onChange={(e) => updateForm({ dogFriendly: e.target.value })}
                />
                <RadioGroup
                    label="Is the pet friendly to cats? "
                    name="catFriendly"
                    options={[
                        { label: "Yes", value: "Yes" },
                        { label: "No", value: "No" },
                        { label: "Unknown", value: "Unknown" }
                    ]}
                    selectedValue={form.catFriendly}
                    onChange={(e) => updateForm({ catFriendly: e.target.value })}
                />
                <RadioGroup
                    label="Is the pet friendly to kids? "
                    name="kidFriendly"
                    options={[
                        { label: "Yes", value: "Yes" },
                        { label: "No", value: "No" },
                        { label: "Unknown", value: "Unknown" }
                    ]}
                    selectedValue={form.kidFriendly}
                    onChange={(e) => updateForm({ kidFriendly: e.target.value })}
                />
                    
                <FormInput
                    label="Rescue Name:"
                    id="petRescue"
                    value={form.petRescue}
                    onChange={(e) => updateForm({ petRescue: e.target.value })}
                />
                <FormInput
                    label="Rescue Email:"
                    id="rescueEmail"
                    value={form.rescueEmail}
                    onChange={(e) => updateForm({ rescueEmail: e.target.value })}
                />
                

                <div className="form-group">
                    <label htmlFor="arrivalDate">Date of Arrival: </label>
                    <input
                        type="date"
                        className="form-control"
                        id="arrivalDate"
                        onChange={(e) => updateForm({ arrivalDate: e.target.value })}
                    />
                </div>
                <div className="form-group">
                    <label htmlFor="petImage">Upload Pet Image: </label>
                    <input
                        type="file"
                        className="form-control"
                        id="petImage"
                        onChange={onChangeFile}
                    />
                </div>
                {form.petImage && <p>Selected file: {form.petImage.name}</p>}
            
            {/* Submit button */}
            <button type="submit" className="btn btn-primary">Submit</button>

            </form>
        </div>
    );
}
