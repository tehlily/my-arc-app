import React from "react";
import { createRoot } from "react-dom/client";
import App from "./App";
import { BrowserRouter } from "react-router-dom";
import 'bootstrap/dist/css/bootstrap.min.css';
import 'react-toastify/dist/ReactToastify.css';
import { initializeApp } from "firebase/app";
import { getAnalytics } from "firebase/analytics";

const container = document.getElementById('root');
const root = createRoot(container);

const firebaseConfig = {
  apiKey: "AIzaSyCjbZFdh0EJAytlO--SUAN3q8ri8mFkjys",
  authDomain: "arcapp-techwise.firebaseapp.com",
  projectId: "arcapp-techwise",
  storageBucket: "arcapp-techwise.appspot.com",
  messagingSenderId: "728350188872",
  appId: "1:728350188872:web:76a19062079c08098c7f7d",
  measurementId: "G-JPXFMC45M7"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
const analytics = getAnalytics(app);

root.render(
  // <React.StrictMode>
    <BrowserRouter>
      <App />
    </BrowserRouter>
  // </React.StrictMode>
);