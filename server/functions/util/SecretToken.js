const { config } = require("dotenv");
const jwt = require("jsonwebtoken");

const result = config();

if (result.error) {
  console.error("Failed to load environment variables from .env:", result.error.message);
}

exports.createSecretToken = (id) => {
  return jwt.sign({ id }, process.env.TOKEN_KEY, {
    expiresIn: 3 * 24 * 60 * 60,
  });
};
