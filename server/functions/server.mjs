import express from "express";
import cors from "cors";
import cookieParser from "cookie-parser";
import dotenv from "dotenv";
import mongoose from "mongoose";
import path from "path";
import bodyParser from "body-parser";
import petrecordRoutes from "./routes/petrecord.js";
import authRoute from "./routes/AuthRoute.js";
import { fileURLToPath } from 'url';
import axiosServer from './axiosServer.js'; // Importing axiosServer module

dotenv.config();
const app = express();

const Db = process.env.ATLAS_URI;
const PORT = process.env.PORT || 5000;

console.log("Port: " + PORT);

const __filename = fileURLToPath(import.meta.url);
const __dirname = path.dirname(__filename);

const options = {
  useNewUrlParser: true,
  useUnifiedTopology: true,
  dbName: 'arcdata',
};

// Middleware for static files
app.use(express.static(path.join(__dirname, 'build')));

// Configure header info
app.use(cors({
  origin: true,
  methods: ["GET", "POST", "PUT", "PATCH", "DELETE"],
  credentials: true,
}));

app.use(express.json());
app.use(cookieParser());
app.use(bodyParser.json());

// Use your routes
app.use("/api", petrecordRoutes);
app.use("/api", authRoute);

// Example usage of axiosServer (if you need to make server-side requests)
// You can place this inside any endpoint or function as needed
/*
app.get('/some-endpoint', async (req, res) => {
  try {
    const response = await axiosServer.get('/some-api-endpoint');
    res.json(response.data);
  } catch (error) {
    res.status(500).json({ error: 'An error occurred' });
  }
});
*/

// Connect to MongoDB
console.log("Trying to connect to MongoDB");
mongoose.connect(Db, options).then(() => {
  console.log("Connected to MongoDB Atlas successfully!");
  app.listen(PORT, () => {
    console.log(`Server is running on port ${PORT}`);
  });
}).catch((error) => {
  console.error("Error connecting to MongoDB Atlas:", error);
});

// Handle React app's index.html for the root route
// app.get('/', (req, res) => {
//   res.sendFile(path.join(__dirname, 'build', 'index.html'));
// });

// Handle React app's index.html for all other routes (SPA support)
app.get('*', (req, res) => {
  res.sendFile(path.join(__dirname, 'build', 'index.html'));
});
