const express = require('express');
const { Signup, Login } = require('../controllers/AuthController.js');
const { userVerification } = require('../middlewares/AuthMiddleware.js');
const axiosServer = require('../axiosServer.js');
const User = require('../models/UserModel.js');

const router = express.Router();

router.post("/signup", Signup);
router.post("/login", Login);
router.post('/user_home', userVerification);

router.get('/user_data', userVerification, (req, res) => {
    res.json({ status: true, user: req.user.username });
});

// Example endpoint using axiosServer
router.get('/external_data', async (req, res) => {
    try {
        const response = await axiosServer.get('/some-external-endpoint');
        res.json(response.data);
    } catch (error) {
        res.status(500).json({ error: 'An error occurred' });
    }
});

module.exports = router;
