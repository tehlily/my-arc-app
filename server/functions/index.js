const express = require("express");
const cors = require("cors");
const cookieParser = require("cookie-parser");
const dotenv = require("dotenv");
const mongoose = require("mongoose");
const path = require("path");
const bodyParser = require("body-parser");
const petrecordRoutes = require("./routes/petrecord.js");
const authRoute = require("./routes/AuthRoute.js");
const axiosServer = require('./axiosServer.js');
const functions = require('firebase-functions');

dotenv.config();
const app = express();

// const Db = process.env.ATLAS_URI;
const Db = functions.config().mongodb.uri;
const PORT = process.env.PORT || 5000;

console.log("From index.js: Port: " + PORT);

__filename = __filename || require('url').fileURLToPath(require('url').pathToFileUrl(__dirname));
__dirname = path.dirname(__filename);

const options = {
  useNewUrlParser: true,
  useUnifiedTopology: true,
  dbName: 'arcdata',
};

// Middleware for static files
app.use(express.static(path.join(__dirname, '../build')));

// Configure header info
app.use(cors({
  origin: true,
  methods: ["GET", "POST", "PUT", "PATCH", "DELETE"],
  credentials: true,
}));

app.use(express.json());
app.use(cookieParser());
app.use(bodyParser.json());

// Use your routes
app.use("/api", petrecordRoutes);
app.use("/api", authRoute);

// Example usage of axiosServer (if you need to make server-side requests)
/*
app.get('/some-endpoint', async (req, res) => {
  try {
    const response = await axiosServer.get('/some-api-endpoint');
    res.json(response.data);
  } catch (error) {
    res.status(500).json({ error: 'An error occurred' });
  }
});
*/

// Connect to MongoDB
console.log("Trying to connect to MongoDB");
mongoose.connect(Db, options).then(() => {
  console.log("Connected to MongoDB Atlas successfully!");
}).catch((error) => {
  console.error("Error connecting to MongoDB Atlas:", error);
});

// Handle React app's index.html for the root route
// app.get('/', (req, res) => {
//   res.sendFile(path.join(__dirname, '../build', 'index.html'));
// });

// Handle React app's index.html for all other routes (SPA support)
app.get('*', (req, res) => {
  res.sendFile(path.join(__dirname, '../build', 'index.html'));
});

// Export the app as a Firebase function
exports.app = functions.https.onRequest(app);
