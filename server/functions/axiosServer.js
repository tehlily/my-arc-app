const axios = require('axios');

const axiosServer = axios.create({
  baseURL: 'https://arcapp-techwise.web.app',
  timeout: 5000,
  headers: {
    'Content-Type': 'application/json',
    // Add other default headers if necessary
  },
});

module.exports = axiosServer;