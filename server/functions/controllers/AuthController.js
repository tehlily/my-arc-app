const UserModel = require("../models/UserModel.js");
const { createSecretToken } = require("../util/SecretToken.js");
const bcrypt = require("bcryptjs");

exports.Signup = async (req, res) => {
  try {
    const { email, password, username, createdAt } = req.body;
    const existingUser = await UserModel.findOne({ email });
    if (existingUser) {
      return res.json({ message: "User already exists" });
    }
    const user = await UserModel.create({ email, password, username, createdAt });
    const token = createSecretToken(user._id);
    res.cookie("token", token, {
      withCredentials: true,
      httpOnly: false,
    });
    res.json({ message: "User signed in successfully", success: true, user });
    
  } catch (error) {
    console.error(error);
    res.json({ message: "Error in Signup" });
  }
};

exports.Login = async (req, res) => {
  try {
    const { email, password } = req.body;
    if (!email || !password) {
      return res.json({ message: 'All fields are required' });
    }
    const user = await UserModel.findOne({ email });
    if (!user) {
      return res.json({ message: 'Incorrect password or email' });
    }
    const auth = await bcrypt.compare(password, user.password);
    if (!auth) {
      return res.json({ message: 'Incorrect password or email' });
    }
    const token = createSecretToken(user._id);
    if (!token) {
      return res.json({ message: "Token not created" });
    }
    res.cookie("token", token, {
      withCredentials: true,
      httpOnly: false,
    });
    res.json({ message: "User logged in successfully", success: true, user });
    
  } catch (error) {
    console.error(error);
    res.json({ message: "Uncaught internal server error" });
  }
};
