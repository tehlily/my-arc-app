const mongoose = require("mongoose");

const petRecordSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true
    },
    species: {
        type: String,
        required: true,
        default: 'Unknown'
    },
    breed: {
        type: String,
        required: true,
        default: 'Unknown'
    },
    color: {
        type: String,
        required: true,
        default: 'Unknown'
    },
    ageNum: {
        type: Number,
        required: true
    },
    ageUnit: {
        type: String,
        required: true
    },
    sexGen: {
        type: String,
        required: false,
        default: 'Unknown'
    },
    sexAlt: {
        type: String,
        required: false,
        default: 'Unknown'
    },
    dogFriendly: {
        type: String,
        required: false,
        default: 'Unknown'
    },
    catFriendly: {
        type: String,
        required: false,
        default: 'Unknown'
    },
    kidFriendly: {
        type: String,
        required: false,
        default: 'Unknown'
    },
    petRescue: {
        type: String,
        required: true
    },
    rescueEmail: {
        type: String,
        required: true
    },
    arrivalDate: {
        type: Date,
        required: false
    },
    petImage: String
}, { 
    collection: "petrecords",
    timestamps: true });

const PetRecord = mongoose.model("PetRecord", petRecordSchema);

module.exports = PetRecord;
