import mongoose from "mongoose";
import dotenv from "dotenv";
dotenv.config();
const Db = process.env.ATLAS_URI;

try {
    console.log("Connecting to MongoDB Atlas...");
    await mongoose.connect(Db, {
        useNewUrlParser: true,
        useUnifiedTopology: true,
    });
    // console.log("Connected to MongoDB Atlas successfully!");
} catch (error) {
    console.error("Error connecting to MongoDB Atlas:", error);
}

mongoose.connection.useDb("arcdata");

export default mongoose.conneciton;