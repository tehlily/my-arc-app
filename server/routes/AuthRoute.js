import { Router } from 'express';
import { Signup, Login } from "../../controllers/AuthController.js";
import { userVerification } from '../../middlewares/AuthMiddleware.js';
import axiosServer from '../../axiosServer.js';
import User from "../../models/UserModel.js";

const router = Router()

router.post("/signup", Signup);
router.post("/login", Login);
router.post('/user_home', userVerification);

router.get('/user_data', userVerification, (req, res) => {
    res.json({status: true, user: req.user.username });
});


// Example endpoint using axiosServer
router.get('/external_data', async (req, res) => {
    try {
      const response = await axiosServer.get('/some-external-endpoint');
      res.json(response.data);
    } catch (error) {
      res.status(500).json({ error: 'An error occurred' });
    }
  });

export default router;