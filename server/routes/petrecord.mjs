import express from "express";
import multer from "multer";
import path from "path";
import PetRecord from "../../models/PetRecord.js";
import axiosServer from "../../axiosServer.js";

const petrecordRoutes = express.Router();
const __filename = new URL(import.meta.url).pathname;
const __dirname = path.dirname(__filename);
const storage = multer.diskStorage({
  destination: (req, file, callback) => {
    callback(null, path.join(__dirname, "../../client/public/uploads/"));
  },
  filename: (req, file, callback) => {
    const uniqueSuffix = Date.now() + '-' + Math.round(Math.random() * 1E9);
    callback(null, file.fieldname + '-' + uniqueSuffix);
  }
});

const upload = multer({ storage: storage });

petrecordRoutes.get("/pet_record_list/", async (req, res) => {
  try {
    const petRecords = await PetRecord.find();
    res.status(200).send(petRecords);
  } catch (error) {
    console.error(error);
    res.status(500).send("Internal Server Error");
  }
});

petrecordRoutes.get("/view_pet/:id", async (req, res) => {
  try {
    const petRecord = await PetRecord.findById(req.params.id);
    if (!petRecord) return res.status(404).send("Pet Record not found");
    res.status(200).send(petRecord);
  } catch (error) {
    console.error(error);
    res.status(500).send("Internal Server Error");
  }
});

petrecordRoutes.post("/petrecord/add", upload.single("petImage"), async (req, res) => {
  try {
    const newPetRecord = new PetRecord({
      name: req.body.name,
      ageNum: req.body.ageNum,
      ageUnit: req.body.ageUnit,
      sexGen: req.body.sexGen,
      sexAlt: req.body.sexAlt,
      dogFriendly: req.body.dogFriendly,
      catFriendly: req.body.catFriendly,
      kidFriendly: req.body.kidFriendly,
      petRescue: req.body.petRescue,
      rescueEmail: req.body.rescueEmail,
      petImage: req.file ? req.file.originalname : null,
    });
    await newPetRecord.save();
    res.sendStatus(204);
  } catch (error) {
    console.error(error);
    res.status(500).send("Internal Server Error");
  }
});

petrecordRoutes.patch("/update/:id", upload.single("petImage"), async (req, res) => {
  try {
    const petRecord = await PetRecord.findById(req.params.id);
    if (!petRecord) return res.status(404).send("Pet Record not found");
    const updates = {
      name: req.body.name,
      species: req.body.species,
      breed: req.body.breed,
      color: req.body.color,
      ageNum: req.body.ageNum,
      ageUnit: req.body.ageUnit,
      sexGen: req.body.sexGen,
      sexAlt: req.body.sexAlt,
      dogFriendly: req.body.dogFriendly,
      catFriendly: req.body.catFriendly,
      kidFriendly: req.body.kidFriendly,
      petRescue: req.body.petRescue,
      rescueEmail: req.body.rescueEmail,
      arrivalDate: req.body.arrivalDate,
      petImage: req.file ? req.file.originalname : null,
    };
    await PetRecord.findByIdAndUpdate(req.params.id, updates);
    res.sendStatus(200);
  } catch (error) {
    console.error(error);
    res.status(500).send("Internal Server Error");
  }
});

petrecordRoutes.delete("/:id", async (req, res) => {
  try {
    const petRecord = await PetRecord.findById(req.params.id);
    if (!petRecord) return res.status(404).send("Pet Record not found");
    await PetRecord.findByIdAndDelete(req.params.id);
    res.sendStatus(200);
  } catch (error) {
    console.error(error);
    res.status(500).send("Internal Server Error");
  }
});

// Example usage of axiosServer within a route
petrecordRoutes.get('/external_data', async (req, res) => {
  try {
    const response = await axiosServer.get('/some-external-endpoint');
    res.json(response.data);
  } catch (error) {
    res.status(500).json({ error: 'An error occurred' });
  }
});

export default petrecordRoutes;
