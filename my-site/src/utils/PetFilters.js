const filterMapping = {
    'catFriendly': 'catFriendly',
    'dogFriendly': 'dogFriendly',
    'kidFriendly': 'kidFriendly',
    // Add other mappings as needed
};

export const applyFilters = (pets, filters) => {
    return pets.filter(pet => {
        for (let key in filters) {
            if (filters[key] !== 'any' && pet[filterMapping[key]] !== filters[key]) {
                return false;
            }
        }
        return true;
    });
};
