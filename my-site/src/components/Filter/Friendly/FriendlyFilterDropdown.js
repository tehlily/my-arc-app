import React, { useState } from 'react';

import FriendlyInput from './FriendlyInput';

export default function FriendlyFilterDropDown(props) {
    const { currentFriendlies, friendlyQuery } = props;

    const [friendlies, setFriendlies] = useState([
        { label: "Kids", value: 'kids'},
        { label: "Dogs", value: 'dogs'},
        { label: "Cats", value: 'cats'},        
    ]);

    return (
        <>
            <div>
                Friendly With...
            </div>

            {friendlies.map((friendly, index) => (
                <FriendlyInput
                    key={index}
                    currentFriendlies={currentFriendlies}
                    friendlyQuery={friendlyQuery}
                    friendly={friendly}
                    index={index}
                />
            ))}
        </>
    )
}