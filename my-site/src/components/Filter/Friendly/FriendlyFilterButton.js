import React from 'react';

export default function FriendlyFilterButton(props) {
    const { multiSelectExpanded, setMultiSelectExpanded } = props;

    return (
        <>
            <div
                className='friendly-filter-dropdown'
                onClick={() => setMultiSelectExpanded(!multiSelectExpanded)}
                style={{ backgroungColor: multiSelectExpanded ? '#fff' : ''}}
            >
                <span className='friendly-filter-title'>Friendly Filter</span>

            </div>
        </>
    )
}