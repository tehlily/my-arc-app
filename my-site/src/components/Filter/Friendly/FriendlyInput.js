import React from 'react';

export default function FriendlyInput(props) {
    const { currentFriendlies, friendlyQuery, friendly, index } = props;

    function handleChange(event) {
        if (event.target.checked) {
            friendlyQuery([ ...currentFriendlies, event.target.value])
        } else {
            friendlyQuery((prevState) =>
                prevState.filter((prevItem) => prevItem !== event.target.value)
            )
        }
    }

    function checkedInput(value) {
        return currentFriendlies.includes(value)
    }

    return (
        <>
            <div key={index} className='multi-select'>
                <label>{friendly.label}</label>
                <input
                    ky={index}
                    type="checkbox"
                    name="friendly"
                    checked={checkedInput(friendly.value)}
                    value={friendly.value}
                    className="multi-select-input"
                    onChange={handleChange} />
            </div>
        </>
    )
}