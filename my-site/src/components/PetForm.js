import React from 'react';
import { Formik, Field, Form } from 'formik';

export const CreatePetForm = ({ formik }) => {
    // console.log("formik in CreatePetForm: ", formik);
    return (
        <div className="centered-container">
            <hi>Add A Pet Looking for A Foster:</hi>
        <Formik
            initialValues={formik.data} 
            onSubmit = {formik.handleSubmit}
        >
            <Form className="centered-container">
                <label htmlFor="name" className="form-group">Pet Name: </label>
                <Field id="name" name="name"/>
                <label htmlFor="species" className="form-group">Pet Species: </label>
                <Field id="species" name="species"/>
                <label htmlFor="breed" className="form-group">Pet Breed: </label>
                <Field id="breed" name="breed"/>
                <label htmlFor="color" className="form-group">Pet Color: </label>
                <Field id="color" name="color"/>
                <label htmlFor="ageNum" className="form-group">Pet Age: </label>
                <Field id="ageNum" name="ageNum" type="number" min="1" step="1"/>
                <div className="form-group">
                    <div id="age-units-options">Pet Age Units: </div>
                    <label>
                        <Field type="radio" name="ageUnit" value="Weeks" />
                        Weeks
                    </label>
                    <label>
                        <Field type="radio" name="ageUnit" value="Months" />
                        Months
                    </label>
                    <label>
                        <Field type="radio" name="ageUnit" value="Years" />
                        Years
                    </label>
                </div>
                <div className="form-group">
                    <div id="gen-sex-options">Pet Sex: </div>
                    <label>
                        <Field type="radio" name="sexGen" value="Male" />
                        Male
                    </label>
                    <label>
                        <Field type="radio" name="sexGen" value="Female" />
                        Female
                    </label>
                    <label>
                        <Field type="radio" name="sexGen" value="Unknown" />
                        Unknown
                    </label>
                </div>
                <div className="form-group">
                    <div id="alt-sex-options">Is the Pet Spayed/Neutered? </div>
                    <label>
                        <Field type="radio" name="sexAlt" value="Yes" />
                        Yes
                    </label>
                    <label>
                        <Field type="radio" name="sexAlt" value="No" />
                        No
                    </label>
                    <label>
                        <Field type="radio" name="sexAlt" value="Unknown" />
                        Unknown
                    </label>
                </div>
                <div className="form-group">
                    <div id="dog-friendly-options">Is the Pet Friendly with Most Dogs? </div>
                    <label>
                        <Field type="radio" name="dogFriendly" value="Yes" />
                        Yes
                    </label>
                    <label>
                        <Field type="radio" name="dogFriendly" value="No" />
                        No
                    </label>
                    <label>
                        <Field type="radio" name="dogFriendly" value="Unknown" />
                        Unknown
                    </label>
                </div>
                <div className="form-group">
                    <div id="cat-friendly-options">Is the Pet Friendly with Most Cats? </div>
                    <label>
                        <Field type="radio" name="catFriendly" value="Yes" />
                        Yes
                    </label>
                    <label>
                        <Field type="radio" name="catFriendly" value="No" />
                        No
                    </label>
                    <label>
                        <Field type="radio" name="catFriendly" value="Unknown" />
                        Unknown
                    </label>
                </div>
                <div className="form-group">
                    <div id="kid-friendly-options">Is the Pet Friendly with Most Kids? </div>
                    <label>
                        <Field type="radio" name="kidFriendly" value="Yes" />
                        Yes
                    </label>
                    <label>
                        <Field type="radio" name="kidFriendly" value="No" />
                        No
                    </label>
                    <label>
                        <Field type="radio" name="kidFriendly" value="Unknown" />
                        Unknown
                    </label>
                </div>
                <label htmlFor="petRescue" className="form-group">Rescue Name: </label>
                <Field id="petRescue" name="petRescue"/>
                <label htmlFor="rescueEmail" className="form-group">Rescue Email: </label>
                <Field id="rescueEmail" name="rescueEmail"/>
                <label htmlFor="arrivalDate" className="form-group">Expected Date of Arrival: </label>
                <Field id="arrivalDate" name="arrivalDate" type="date"/>
                
                <div className="form-group">
                <label htmlFor="file">Upload Image: </label>
                <input
                    type="file"
                    filename="petImage"
                    accept="image/*"
                    className="form-control-file"
                    id="petImage"
                    // onChange={onChangeFile}
                />
                {formik.values.petImage && <p>Selected file: {formik.values.petImage.name}</p>}
            </div>
                <button type="submit">Submit</button>
            </Form>
        </Formik>
        </div>
    );
};



//             name: "",
//             species: defaultAnswer,
//             breed: defaultAnswer,
//             color: defaultAnswer,
//             ageNum: "",
//             ageUnit: "",
//             sexGen: defaultAnswer,
//             sexAlt: defaultAnswer,
//             dogFriendly: defaultAnswer,
//             catFriendly: defaultAnswer,
//             kidFriendly: defaultAnswer,
//             petRescue: "",
//             rescueEmail: "",
//             arrivalDate: null,
//             petImage: null,