import React from 'react';

const RadioGroup = ({ label, options, name, selectedValue, onChange }) => (
    <div className="form-group">
        <label>{label}  </label>
        {options.map(option => (
            <div className="form-check form-check-inline" key={option.value}>
                <input
                    className="form-check-input"
                    type="radio"
                    name={name}
                    id={`${name}${option.value}`}
                    value={option.value}
                    checked={selectedValue === option.value}
                    onChange={onChange}
                />
                <label htmlFor={`${name}${option.value}`} className="form-check-label">
                    {option.label}
                </label>
            </div>
        ))}
    </div>
);

export default RadioGroup;
