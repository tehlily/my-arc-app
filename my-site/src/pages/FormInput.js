import React from 'react';

const FormInput = ({ label, type = "text", value, onChange, id, ...props }) => (
    <div className="form-group">
        <label htmlFor={id}>{label} </label>
        <input
            type={type}
            className="form-control"
            id={id}
            value={value}
            onChange={onChange}
            {...props}
        />
    </div>
);

export default FormInput;
